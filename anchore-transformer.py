#!/usr/bin/env python3

import json
import sys
import hashlib

gitlab_sev_map = {
    "critical": "Critical",
    "high": "High",
    "medium": "Medium",
    "low": "Low",
    "negligible": "Info",
    "unknown": "Info",
}

gitlab_sast_template = {
    "version": "2.3",
    "vulnerabilities": [],
    "remediations": []
}

input_file = sys.argv[1]
image = sys.argv[2]

with open(input_file, 'r') as FH:
    vulns = json.loads(FH.read())

gitlab_sast = {}
gitlab_sast.update(gitlab_sast_template)

for vuln in vulns:
    vid = vuln.get('vulnerability', {}).get('id')
    vsev = vuln.get('vulnerability', {}).get('severity', "unknown")
    gvsev = gitlab_sev_map.get(vsev.lower(), "Info")
    desc = vuln.get('vulnerability', {}).get('description', "Unavailable")
    vlinks = vuln.get('vulnerability', {}).get('links', [])
    matcher = vuln.get('matched-by', {}).get('matcher', "unknown")
    
    pkgname = vuln.get('artifact', {}).get('name')
    pkgvers = vuln.get('artifact', {}).get('version')
    pkglocs = vuln.get('artifact', {}).get('locations', [])
    if pkglocs:
        pkgloc = pkglocs[0].get('path', "NA")
    else:
        pkgloc = "NA"    
    pkg = "{}-{}".format(pkgname, pkgvers)

    
    scanner = "anchore-grype"
    vhash = hashlib.sha256(bytes("{}-{}-{}".format(vid, pkg, pkgloc), "UTF-8")).hexdigest()

    if vlinks:
        vlink = vlinks[0]
    else:
        vlink = ""
        
    gvuln = {
#        "id": vhash,
        "cve": "{}:{}:{}".format(image, pkg, vid),
        "category": "container_scanning",
        "message": "{} in {}".format(vid, pkg),
        "description": desc,
        "severity": gvsev,
        "confidence": "Unknown",
        "solution": "Unknown",
        "scanner": {
            "id": scanner,
            "name": scanner
        },
        "location": {
            "dependency": {
                "package": {
                    "name": "{}".format(pkgname)
                },
                "version": pkgvers
            },
            "operating_system": matcher,
            "image": image
        },
        "identifiers": [
            {
                "type": "cve",
                "name": vid,
                "value": vid,
                "url": vlink
            }
        ],
        "links": []
    }
    for vl in vlinks:
        gvuln['links'].append(
            {
                "url": vl
            }
        )
    gitlab_sast['vulnerabilities'].append(gvuln)

with open("output.json", 'w') as OFH:
    OFH.write(json.dumps(gitlab_sast, indent=4))
